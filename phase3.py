import requests
from bs4 import BeautifulSoup
import csv
from pathlib import Path
import logging
logging.basicConfig(level=logging.INFO)

#recuperation de l'url du site book to scrape
url_web='http://books.toscrape.com/index.html'

#recuperation du code html de la page selectionnee
response = requests.get(url_web)
doc=BeautifulSoup(response.content, "html.parser")

#recuperation des liens de category et suppression du premier car inutile
nav_list = doc.find("ul", class_="nav-list")
category_links = []
for a_tag in nav_list.find_all("a"):
    link = a_tag.get("href")
    category_links.append("http://books.toscrape.com/" + link)
del(category_links[0])

#creation des listes pour acceuillir les donnees
product_page_url=[]
universal_product_code=[]
title=[]
price_including_tax=[]
price_excluding_tax=[]
number_available=[]
product_description=[]
category=[]
review_rating=[]
image_url=[]
count=1

for link in category_links:
    
    #recuperation du code html de la page selectionnee
    response = requests.get(link)
    doc=BeautifulSoup(response.content, "html.parser")

    #test pour savoir s'il y a plusieurs page categories par exemple Historical Fiction qui a deux pages
    if doc.find('li',{'class':'next'}):
        total_pages = int(doc.select(".pager .current")[0].text.strip().split()[-1])
        base_url = link.replace('index.html','')
        url_livres=[]
        for page in range(1, total_pages + 1):
            page_url = f"{base_url}page-{page}.html"
            response = requests.get(page_url)
            doc = BeautifulSoup(response.content, "html.parser")
            for link in doc.find_all('div',{'class':'image_container'}):
                url_livres.append(link.find_next('a').get('href').replace('../../../', 'http://books.toscrape.com/catalogue/'))
    else :
        url_livres=[]
        for link in doc.find_all('div',{'class':'image_container'}):
            url_livres.append(link.find_next('a').get('href').replace('../../../', 'http://books.toscrape.com/catalogue/'))

    #creation d'une boucle pour chaque livre
    for link in url_livres:

        #recuperation du code html de la page selectionnee
        response = requests.get(link)
        doc=BeautifulSoup(response.content, "html.parser")

        #recuperation de l'url
        product_page_url.append(doc.find('th',string='UPC').find_next('td').string)

        #recuperation du code upc
        universal_product_code.append(doc.find('th',string='UPC').find_next('td').string)

        #recuperation du titre
        title.append(doc.find('h1').string)

        #recuperation de prix avec taxes
        price_including_tax.append(doc.find('th',string='Price (incl. tax)').find_next('td').string)

        #recuperation de prix sans taxes
        price_excluding_tax.append(doc.find('th',string='Price (excl. tax)').find_next('td').string)
    
        #recuperation du stock disponible et suppression du texte en trop
        number_available.append(doc.find('th',string='Availability').find_next('td').string.replace("In stock ","").replace("(","").replace(")","").replace(" available",""))

        #test pour savoir si la description est vide ou non et recupere la description si oui
        product_description_test = doc.find('div',{'id':'product_description'})
        if product_description_test is not None and product_description_test.find_next('p') is not None:
            #Vérifie si la balise de description du produit existe et si elle a une balise "p" suivante
            product_description.append('"' +product_description_test.find_next('p').string + '"')
        else:
            product_description.append('"' + '' + '"')
    
        #recuperation de la categorie du livre
        category.append(doc.find('li', {'class':'active'}).find_previous('a').string)

        #recuperation de la notation du livre
        review_rating_book=doc.find('div', {"class":'col-sm-6 product_main'}).find_next('p',{'class':'star-rating'})

        #creation d'un test permettant de voir la notation /5, zero est ajoute au cas ou, mais il ne semble pas present sur le site Book To Scrape
        if "Zero" in str(review_rating_book):
            review_rating_book=0
        elif "One" in str(review_rating_book):
            review_rating_book=1
        elif "Two" in str(review_rating_book):
            review_rating_book=2
        elif "Three" in str(review_rating_book):
            review_rating_book=3
        elif "Four" in str(review_rating_book):
            review_rating_book=4
        else:
            review_rating_book=5
    
        review_rating.append(review_rating_book)

        #recuperation de l'url de l'image du livre et on remplace '../../' par 'http://books.toscrape.com/' pour avoir un lien url valide
        image_url.append(doc.find('img').get('src').replace("../../","http://books.toscrape.com/"))
        logging.info(f"livre N {count}")
        count +=1


#creation d'un header pour le fichier csv
header = ["product page url", "universal product code", "title" ,"price including tax", "price excluding tax", "number available", "product description", "category", "review rating", "image url"]

#recuperation des données dans une variable
data = [product_page_url,universal_product_code, title,price_including_tax, price_excluding_tax, number_available, product_description, category, review_rating, image_url]

#obtention du chemin du dossier dans lequel se trouve le script
script_dir = Path(__file__).resolve().parent

#creation d'une boucle pour ajouter les donnees dans le fichier csv
data_rows = []
for i in range(len(data[0])):
    data_row = []
    for j in range(len(data)):
        data_row.append(data[j][i])
    data_rows.append(data_row)

#creation du fichier csv
csv_file = script_dir / "phase3.csv"
with csv_file.open(mode='w', newline='') as f:
    writer = csv.writer(f,delimiter=',')

    #ecriture du header
    writer.writerow(header)

    #ecriture des donnees
    for row in data_rows:
        writer.writerow(row)