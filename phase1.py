import requests
from bs4 import BeautifulSoup
import csv
from pathlib import Path

#recuperation de l'url de la page du livre pour extraire les donnees
product_page_url='http://books.toscrape.com/catalogue/its-only-the-himalayas_981/index.html'

#recuperation du code html de la page selectionnee
response = requests.get(product_page_url)
doc=BeautifulSoup(response.content, "html.parser")

#recuperation du code UPC
universal_product_code=doc.find('th',string='UPC').find_next('td').string

#recuperation du titre
title=doc.find('h1').string

#recuperation de prix avec taxes
price_including_tax=doc.find('th',string='Price (incl. tax)').find_next('td').string

#recuperation de prix sans taxes
price_excluding_tax=doc.find('th',string='Price (excl. tax)').find_next('td').string

#recuperation du stock disponible et suppression du texte en trop
number_available=doc.find('th',string='Availability').find_next('td').string.replace("In stock ","").replace("(","").replace(")","").replace(" available","")

#recuperation de la description du produit
product_description=doc.find('div',{'id':'product_description'}).find_next('p').get_text(strip=True).replace('\u203d','!?')

#recuperation de la categorie du livre
category=doc.find('li', {'class':'active'}).find_previous('a').string

#recuperation de la notation du livre
review_rating=doc.find('div', {"class":'col-sm-6 product_main'}).find_next('p',{'class':'star-rating'})
review_rating = doc.find('p', {'class': 'star-rating'})['class'][1]

#creation d'un test permettant de voir la notation /5, zero est ajoute au cas ou, mais il ne semble pas present sur le site Book To Scrape
if "Zero" in str(review_rating):
    review_rating=0
elif "One" in str(review_rating):
    review_rating=1
elif "Two" in str(review_rating):
    review_rating=2
elif "Three" in str(review_rating):
    review_rating=3
elif "Four" in str(review_rating):
    review_rating=4
else:
    review_rating=5

#recuperation de l'url de l'image du livre et on remplace '../../' par 'http://books.toscrape.com/' pour avoir un lien url valide
image_url=doc.find('img').get('src').replace("../../","http://books.toscrape.com/")

#creation d'un header pour le fichier csv
header = ["product page url", "universal product code", "title" ,"price including tax", "price excluding tax", "number available", "product description", "category", "review rating", "image url"]

#recuperation des données dans une variable
data = [product_page_url,universal_product_code, title,price_including_tax, price_excluding_tax, number_available, product_description, category, review_rating, image_url]

#obtention du chemin du dossier dans lequel se trouve le script
script_dir = Path(__file__).resolve().parent

#creation du fichier csv
csv_file = script_dir / "phase1.csv"
with csv_file.open(mode='w', newline='') as f:
    writer = csv.writer(f,delimiter=',')

    #ecriture du header
    writer.writerow(header)

    #ecriture des donnees
    writer.writerow(data)