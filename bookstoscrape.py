#librairy requiered
import requests
from bs4 import BeautifulSoup
import csv
from pathlib import Path

#getting path file and creating folder image to download images
script_dir = Path(__file__).resolve().parent
image_dir = script_dir / "image"
#testing if image folder already exist or not
image_dir.mkdir(parents=True, exist_ok=True) 

#picking up bookstoscrape url and creating a session request
base_url = 'https://books.toscrape.com'
# using request.session instead of request.get to obtain all informations researched in a single librairy and gain time
session = requests.Session() 

def extract_book_data(response,doc):
    # return all informations seeked for a book
    product_page_url = response.url
    universal_product_code = doc.find('th', string='UPC').find_next('td').string
    title = doc.find('h1').string
    price_including_tax = doc.find('th', string='Price (incl. tax)').find_next('td').string
    price_excluding_tax = doc.find('th', string='Price (excl. tax)').find_next('td').string
    number_available = doc.find('th', string='Availability').find_next('td').string.replace("In stock ","").replace("(","").replace(")","").replace(" available","")  #replacing text that is unnecessary, only looking for a number
    product_description_test = doc.find('div', {'id':'product_description'})
    # looking if the description is empty or not, if yes, adding empty description, if not, adding the description
    if product_description_test is not None and product_description_test.find_next('p') is not None: 
        product_description = product_description_test.find_next('p').string
    else:
        product_description = ''
    category = doc.find('li', {'class':'active'}).find_previous('a').string
    review_rating = doc.find('p', {'class': 'star-rating'})['class'][1]
    image_url = doc.find('img').get('src').replace("../../","http://books.toscrape.com/")

    return {
        'product_page_url': product_page_url,
        'universal_product_code': universal_product_code,
        'title': title,
        'price_including_tax': price_including_tax,
        'price_excluding_tax': price_excluding_tax,
        'number_available': number_available,
        'product_description': product_description,
        'category': category,
        'review_rating': review_rating,
        'image_url': image_url
    }

def get_category_links(base_url):
    # return all category links
    response = session.get(base_url)
    doc = BeautifulSoup(response.text, 'html.parser')
    category_list = doc.find("ul", class_="nav-list")
    category_links = []
    for category in category_list.find_all("a"):
        link = category.get("href")
        # looking if the category link is the same as the main page of bookstoscrape.com which is not interresting
        if link != "catalogue/category/books_1/index.html": 
            category_links.append("http://books.toscrape.com/" + link)
    return category_links

def create_csv_file(csv_file):
    with csv_file.open('w', newline='') as f:
        fieldnames = ['product_page_url', 'universal_product_code', 'title', 'price_including_tax', 'price_excluding_tax', 'number_available', 'product_description', 'category', 'review_rating', 'image_url']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

def generate_category_csv_filename(category_name):
    # generate a CSV filename based on the category name
    return script_dir / f"{category_name}.csv"

def download_images(book_data, image_dir):
    # return images with upc code as name in the folder image
    filename = f"{book_data['universal_product_code']}.jpg"
    with open(image_dir / filename, "wb") as f:
        response = session.get(book_data['image_url'])
        f.write(response.content)

def extract_all_books_data(category_links):
    for category_link in category_links:
        category_base_url = category_link
        response = session.get(category_link)
        doc = BeautifulSoup(response.content, "html.parser")
        category_name = doc.find('h1').string.strip()
        csv_file = generate_category_csv_filename(category_name)
        create_csv_file(csv_file)
        
        # Checking if book category links page has multiple pages and adding them to "url_livres"
        if doc.find('li', {'class': 'next'}):
            # Obtaining the number of pages the category has if there is a next button
            total_pages = int(doc.select(".pager .current")[0].text.strip().split()[-1])
            base_url = category_base_url.replace('index.html', '')
            url_livres = []
            # Browsing the category pages and adding them to a list named "url_livres"
            for page in range(1, total_pages + 1):
                page_url = f"{base_url}page-{page}.html"
                response = session.get(page_url)
                doc = BeautifulSoup(response.content, "html.parser")
                for link in doc.find_all('div', {'class': 'image_container'}):
                    url_livres.append(link.find_next('a').get('href').replace('../../../', 'http://books.toscrape.com/catalogue/'))
        else:
            url_livres = []
            for link in doc.find_all('div', {'class': 'image_container'}):
                url_livres.append(link.find_next('a').get('href').replace('../../../', 'http://books.toscrape.com/catalogue/'))

        for link in url_livres:
            response = session.get(link)
            doc = BeautifulSoup(response.content, "html.parser")
            book_data = extract_book_data(response, doc)
            with csv_file.open('a', newline='', encoding='utf-8') as f:
                fieldnames = ['product_page_url', 'universal_product_code', 'title', 'price_including_tax', 'price_excluding_tax', 'number_available', 'product_description', 'category', 'review_rating', 'image_url']
                writer = csv.DictWriter(f, fieldnames=fieldnames)
                writer.writerow(book_data)
            download_images(book_data, image_dir)

category_links = get_category_links(base_url)
extract_all_books_data(category_links)

#informing when the script end
print("csv file has been fully generated and images downloaded.")