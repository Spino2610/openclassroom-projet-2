# Openclassroom Projet 2

## Recap
Ce programme en Python a été développé dans le cadre d'un projet étudiant et vous permet de récupérer les prix des livres chez Books to Scrape, un revendeur de livres en ligne. Il s'agit d'une version bêta qui vous permettra d'extraire les informations tarifaires au moment de l'exécution.

Il y a trop de livre et trop de librairies en ligne, il est compliqué de pouvoir suivre manuellement les différents prix des livres.  
Mon équipe et moi devons automatiser cette tâche en récupérant les prix des différents livres de librairies en lignes.  
En tant qu'analyste marketing chez Books Online, je suis chargé de développer une version bêta de ce système pour suivre les prix chez [`Book To Scrape`](http://books.toscrape.com/) grâce à une application exécutable à la demande visant à récupérer les prix au moment de son exécution.

## Objectif

 * Aller voir le site [`Book To Scrape`](http://books.toscrape.com/), voir sa structure et son code pour mieux comprendre et visualisé ma tâche.
 * Créer un repositery qui doit inclure un `requirements.txt` et un `Readme.md` permettant d'exécuté le code avec succès et produise des données.
 * Commencé a scrapé sur le site pour me familliarisé avec celui-ci.
 * Faire la phase 1 demandée, à savoir : prendre une page de livre, ici la page du livre [`It's Only the Himalayas`](http://books.toscrape.com/catalogue/its-only-the-himalayas_981/index.html), créer un script python pour extraire les informations suivantes :  

    ● product_page_url  
● universal_ product_code (upc)  
● title  
● price_including_tax  
● price_excluding_tax  
● number_available  
● product_description  
● category  
● review_rating  
● image_url  

    Puis extraire les données dans un fichier CSV.

 * Passez à la phase 2, prendre page categorie, ici, la première page, [`Travel`](http://books.toscrape.com/catalogue/category/books/travel_2/index.html). Récupérer les différentes url des livres et les additionner au premier scrip pour prélever les informations de la première phase pour chaque livre sur la catégorie. Puis extraire les données dans un fichier CSV.

 * Passez à la phase 3, extraire toutes les url categorie du site [`Book To Scrape`](http://books.toscrape.com/index.html), le combiner à la phase 2 pour récupérer tous les livres disponibles  

 * Passez à la phase 4 du code, télécharger et enregistrer le fichier
image de chaque page Produit que vous consultez.

## Etape-clés

 * Création du repositery, du `Readme.txt` et du `requirements.txt`.

 * Faire le script python pour extraire les données du site Book To Scrape avec les quatre phases sitées au dessus dans les objectifs.

 * Envoyer un fichier zip des données générées.

 * Envoyer un mail décrivant comment utiliser le code pour établir une pipeline ETL.


## Utilisation du projet

### Prérequis
Avant d'exécuter ce programme, assurez-vous d'avoir les éléments suivants installés sur votre système :
Python 3 : [`Télécharger Python`](https://www.python.org/downloads/)

### Installation
Clonez ce dépôt de code sur votre machine locale ou téléchargez-le au format ZIP.
Accédez au répertoire du projet.

Pour cloner ce projet exécutez la commande suivante : `$ git clone https://gitlab.com/Spino2610/openclassroom-projet-2.git`

### Configuration de l'environnement virtuel
Un environnement virtuel est recommandé pour exécuter ce programme, afin de maintenir les dépendances du projet isolées. Voici comment créer et activer un environnement virtuel :

Ouvrez une console ou un terminal.
Accédez au répertoire du projet si ce n'est pas déjà fait.
Exécutez la commande suivante pour créer un environnement virtuel :

`python3 -m venv env`

Activez l'environnement virtuel avec la commande suivante :  
Sur Linux ou macOS :  
`source env/bin/activate`  
Sur Windows :  
`.\env\Scripts\activate`  

### Installation des dépendances
Une fois l'environnement virtuel activé, vous pouvez installer les dépendances nécessaires en exécutant la commande suivante :
`pip install -r requirements.txt`

### Exécution du code d'application
Pour exécuter le code d'application et récupérer les prix des livres chez Books to Scrape, exécutez la commande suivante :
`python bookstoscrape.py`

Le programme récupérera les informations du site Books to Scrape au moment de son exécution et les affichera dans un fichier csv.